# Nimukaito

## Sinopsys

Code source du site https://www.nimukaito.net/.

## Dépendances

- Ruby 2.4.2

Les gèmes suivances vont être nécessaire:

* Pour générer le contenu du site:

  - nanoc 4.8.10
  - coderay 1.1.2
  - kramdown 1.15.0
  - builder 3.2.3
  - to_lug 1.0.8

* Outils pour valider et déployer:

  - nokogiri 1.8.1
  - w3c_validators 1.3.3
  - i18n 0.9.0

* Analyse statique du code des librairies:

  - rubocop 0.51.0

* Pour faciliter le développement:

  - guard-nanoc 2.1.2

## État de la branche master

[![pipeline status](https://framagit.org/floreal/www.nimukaito.net/badges/master/pipeline.svg)](https://framagit.org/floreal/www.nimukaito.net/commits/master)

## Licence

Le code source de ce site est utilisable solon les termes de la licence
[MIT](LICENCE).
