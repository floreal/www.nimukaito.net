---
title: "À propos"
author_name: "floreal"
---

Ce site est auto-hébergé, c'est à dire que la machine qui vous le sert
est chez moi.

J'utilise [nanoc](https://nanoc.ws/) pour générer le code html. Les
certificats de chiffrement TLS me sont signés par
[Let's encrypt](https://letsencrypt.org/).

Le code du site est [disponible](https://framagit.org/floreal/www.nimukaito.net)
sur [Framagit](https://framagit.org/) (merci [Framasoft](https://framasoft.org/) <3).

Vous pouvez me contacter par:

- [e-mail](mailto:floreal+blog@nimukaito.net)
- [mastodon](https://eldritch.cafe/@floreal)

pour m'adrésser un message chiffré veuillez utiliser ma clé GPG
publique [4096R/F930C084](/F930C084.asc).

Enfin je tiens à remercier mes lecteurs, relecteurs et correcteurs.
