---
author_name: floreal
---

# Nimukaito

Voici un morceau d'internet dans lequel je parle de mon métier, le développement,
et plus généralement de l'informatique. Ainsi que certains de mes loisirs.
J'essaye d'adopter une aproche aussi originale que possible. Mon objectif est
de rendre mes écrits accessible à un large public. En effet, je souhaite diffuser
mes connaissance et réflexions au plus grand nombre. Gardez juste à l'esprit que
je ne suis qu'humain et qu'il m'arrive d'avoir tors. Aussi vous êtes libre de me
corriger soit en m'[écrivant](mailto:floreal+blog@nimukaito.net), soit en me
proposant une [merge request](https://git.framasoft.org/floreal/www.nimukaito.net/merge_requests).

Le contenu hébergé sur ce blog est diponible selon les termes de la licence
Creative Commons CC-By. Ce qui vous autorise à me citer, republier avec ou
sans vos propres modification du moment que vous nommez le ou les auteur-e-s.
