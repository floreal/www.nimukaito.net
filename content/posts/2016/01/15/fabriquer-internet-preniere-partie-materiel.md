---
title: "Fabriquer de l'Internet: Première partie - Matériel"
author_name: floreal
created_at: 2016-01-15 09:15:07
kind: article
tags:
  - Auto-hébergement
  - Matériel
  - Réseau
  - Internet
  - Tutoriel
---

J'ai récemment parlé auto-hébergement avec plusieurs personnes. Sur
twitter ou IRC, on m'a demandé un retour d'expérience, il me semble que
cela devienne une préoccupation autour de moi. Et comme ça fait un peu
plus de deux ans et demi que j'en fait, je me permets de vous livrer un
petit retour d'expérience au travers d'une petite série d'article dont
voici l'épisode pilote ; j'ai beaucoup à dire. 

Aujourd'hui je parlerai des achats que j'ai effectuer pour me procurer
les ressources (matérielles ou administratives) nécessaires à la mise en
place de mon morceau d'Internet à la maison.

J'aborderai donc l'aspect financier de la chose car, il s'agit d'un
investissement, et je vous laisserai seul-e juge du caractère modeste
ou excessif de ce dernier, nous n'avons pas tous les mêmes bourses.
Enfin je ne tiendrai pas compte du coût de l'abonnement contracté avec
mon fournisseur d'accès à Internet : avec ou sans auto-hébergement, j'en
aurais de toute manière un, bien que le fait de jouir d'une connexion
en FTTH[^1] m'a incité au passage à l'acte.

Pour info, voici la liste des services que j'héberge aujourd'hui:

- Serveur de nom de domaine (DNS)
- Serveur de messagerie électronique (e-mail).
- Serveur de messagerie instantanée (jabber).
- Serveur Web

Enfin sachez qu'il existe une solution plus simple : de
[nombreuses associations FAI locaux](https://db.ffdn.org/) proposent
une solution clé en main: [la Brique
Internet](http://labriqueinter.net) et vous aideront à vous y mettre.
N'hésitez pas à les contacter, ces sont des communautés locales, ils
sont là pour vous aider et en en rejoignant un vous ne vous
retrouverez pas seul-e.

Commençons donc par une petite liste de courses:

- Un mini ordinateur et une carte SD
- Un serveur/boîtier NAS et deux disques dur

## Le serveur

Je me suis tout de suite tourné vers un mini-ordinateur: le  Raspberry
Pi Model B. Avec un boîtier transparent et une alimentation secteur/USB,
en comptant les frais de port cela m'avait coûte à l'époque 60 €. 
Ce choix là se justifie par simplement parce qu'il s'agit d'un bon
rapport qualité / prix. Je ne compte pas compte pas faire de gros
calcul scientifique ou miner de la crypto-monaie et donc pour mon
usage, la bête est largement suffisante.

![Raspberri Pi Modèle B](rpi.jpg)

Cependant j'ai quelques critiques à formuler. Tout d'abord il faut
savoir que tous les périphériques (carte réseau, lecteur SD, etc.)
passent par un seul et même bus USB 2.0. Étant derrière une connexion
en Fibre Optique, le Raspberry Pi se révèle être un goulot
d'étranglement. C'est pourquoi je suggérerai plutôt un [Odroid
C1+](https://www.hardkernel.com/shop/odroid-c1/)
ou un
[OLinuxIno](https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXIno-LIME2/open-source-hardware)
mieux conçus, sans ces inconvénients, pour des prix comparables.

![Odroid C1+](odroidc1.jpg)

![OLinuxIno LIME 2](olinuxino.jpg)

À cela, il faut compter entre 10 et 20 € pour une carte SD afin d'y
installer un système d'exploitation

## Stockage externe

J'ai choisi d'investir dans un petit serveur boîtier NAS n'ayant pas
trop confiance en la fiabilité des carte SD pour le stockage des
données sensibles (e-mail par exemple) sur le long terme, c'est ce qui
m'a coûte le plus cher (220€), il s'agit d'un Synology Diskstation DS213J
Conjointement à ce dernier j'ai commandé deux disques durs de 2 To chacun.
J'ai orienté mon choix sur des disques mécaniques « économes en énergie »,
avec une rotation lente (5000 tours / minute). En effet, il n'est pas
nécessaire de chercher d'ultra haute performance au niveau de la lecture et
de l'écriture: La carte réseau a un débit inférieur au disque dur[^2].
Chacun m'ayant coûte 100€ environ.

![Synology DS213J](synology.jpg)

## En conclusion

Enfin j'ai acheté deux noms de domaine, un en .net (14 € / an), et un
en .fr (12 € / an) que je n'utilise pas au final.

En tout et pour tout, l'investissement initial est d'environ 500 €
(plus 26 € par an pour renouveler mes noms de domaine).

Sur cinq ans, sans tenir compte de l'électricité, ni de l'abonnement
Internet, cela vaut à peu près autant qu'un petit serveur dédié auquel
vous n'avez pas accès physiquement, et pour lequel vous n'avez pas de
nom de domaine: 10.45 € par mois (8.35 € de matériel + 2.15 € en nom de
domaine).

[^1]: Fiber To The Home: La fibre arrive dans votre logement.

[^2]: Débit de la carte réseau : 1Gb/s; Débit des disques durs : 3Gbs

