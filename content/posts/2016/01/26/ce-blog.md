---
title: Ce blog
kind: article
created_at: 2016-01-26 16:09:27
author_name: floreal
tags:
  - Internet
  - Réflexions
---

J'ai commencé à écrire ici à cause de mon billet sur la [philosophie
des langages](/posts/2016/01/08/programmation-philosophie-langages/).
En réalité, je l'avais écrit l'année dernière en novembre,
mais par procrastination ou flemme, je ne m'étais pas occupé de l'aspect
du blog ; j'avais alors un contenu sans contenant. Je m'y suis collé
il y a quelques semaines lorsque, je me suis décidé à travailler plus
sérieusement sur ce site, pour le mettre en ligne le 8 janvier dernier.

Et puis je me suis surpris à prendre goût à la rédaction, parfois gauche,
parfois maladroite, mais que j'essaye de garder la plus authentique et 
honnête possible. Je ne prétends pas écrire la vérité ; juste mes opinions
et mon ressenti sur les sujets qui m'intéressent.

Mon second billet devait traiter de la compilation[^1], mais je n'ai pour l'heure
pas assez de matière pour écrire avec pertinence sur le sujet. Bref, j'ai choisi
de m'exprimer plus tard sur ce sujet ; j'avais un sujet plus inspirant à porté de
cervelle: l'auto-hébergement[^2]. J'ai pourtant rechigné à rédiger dessus
car je ne voyais pas bien quoi dire de plus sur le sujet qui n'ait été déjà
formulé ailleurs, et en mieux. D'un autre coté plusieurs personnes m'ont demandé
− encouragé, plutôt − à rapporter mon retour d'expérience et j'ai fais un bilan
pour voir où j'en étais et je me suis rendu compte que j'avais beaucoup à dire sur là
dessus. Alors plutôt que de rédiger un seul billet, j'ai entamé
[une série](/posts/2016/01/15/fabriquer-internet-preniere-partie-materiel/).
Il semble au final que le premier billet de celle-ci ait été plutôt apprécié,
le lien vers ce dernier ayant été partagé plus que je ne m'y attendais, bien que
je n'en sois pas entièrement satisfait.

## Mes visiteur-euse-s et moi

Et là je commence alors à m'interroger sur mon rapport avec toi, ma lectrice,
mon lecteur. Je souhaite te traiter comme mon égal-e, non pas comme une statistique
pour mesurer mon audience. Je ne te vomis pas de publicité à la face[^3], je ne
traque pas ta navigation ni ne la revends pour de maigres revenus. Tes informations,
tes opinions, je préfère que tu me les donne de ton propre chef, je n'ai pas à les
prendre à ton insu. Oh, bien sûr, il me serait très facile d'écrire un petit script qui
va analyser les journaux d'accès à ce blog, de relever l'heure, l'adresse IP, la page
consultée et le navigateur[^4]. Et de dresser de toi un profil approximatif pour mieux
te vendre. À la place, je vais le désactiver, et l'effacer.

On peut également parler des cookies. Là où d'autres te disent que tu n'as pas le
choix, qu'ils te traquent, et que tu n'as pas ton mot à dire.

![Avertissement sur l'utilisation de cookie](cookies.png)

Là où ils t'ordonnent juste d'accepter ou de te casser de leur propriété, je t'invite
chez moi, à me lire et, si tu juges pertinent, intéressant, marrant, à partager, diffuser
ou [me répondre](mailto:floreal+blog@nimukaito.net) si tu n'es pas d'accord ou que tu
veux juste discuter. Si tu veux corriger mes fautes d'orthographe, me proposer une
reformulassions, ou ajouter des fonctionnalités au site, tu peux
[contribuer](https://framagit.org/floreal/www.nimukaito.net), mais je ne t'oblige à
rien. En fait tu peux avoir confiance en moi parce que je ne te la demande pas.

## Commentaires

Cela me conduit à une autre question: Est-ce que je dois te laisser commenter mes
articles ? C'est à dire dois-je, comme sur la majorité des blog, réserver un espace
où tu peux t'exprimer ? J'y vois autant d'atout que d'inconvénient.

Contenu statique
:   Les pages sont générées au moment où je publie un nouvel article ; et non pas quand
:   tu me rends visite. Ce qui fait que je ne peux pas me reposer sur une gestion
:   des commentaires par le site pour te permettre de lire des commentaires frais, ou d'en
:   envoyer de nouveaux. Ceci est contournâmes en ajoutant des appels à un service tiers..

Respecter ta vie privée
:   On m'a suggéré d'utiliser des services tels que disqus. Mais cela va à l'encontre
:   de ce que j'écrivais plus haut au sujet des traqueurs de navigation. Et pus, quitte à ce
:   que tu t'exprimes sur mes billets, autant le faire directement chez moi. Car il existe des
:   solution auto-hébergées.

Je n'ai pas envie de modérer
:   Te permettre donner une voix pose la question de l'édition de tes commentaires
:   par mes soins. En plus ma responsabilité civile ou pénale quant aux petits malins qui
:   laisseraient des messages illicites (incitation à la haine, harcèlement, etc.). 
:   Je dois alors t'accorder ma confiance, à ce propos.

Interactivité et ergonomie
:   Les échanges seront quand même plus simples, puisqu'ils permettent d'utiliser le même
:   médium qui te permet de consulter ce site, plutôt que mon adresse e-mail ou sur les 
:   réseau sociaux par lesquels je m'exprime assez peu, au final.

J'ai pris l'initiative d'ajouter tout de même cette fonctionnalité. Pour un temps en tout cas.
Et si ça marche, que cela ne m'apporte aucun problème, je la garderai.

## Évolution du blog

Donc, [j'ai ajouté](https://git.framasoft.org/floreal/www.nimukaito.net/tree/comments)
un encart dédié à vos commentaires sur chacun des mes billets. Libre à vous de les utiliser
ou pas.

L'ami m'ayant suggéré d'inclure un encart disqus, m'a aussi fait remarqué que les tags desquels
je marque mes billets ne servent à rien. En effet ces derniers jusqu'à présent n'étaient pas
regroupés au sein de pages consacrées à ces mots clé. Je lui ai répondu, que je développerai
cette fonctionnalité là en temps voulu. Et c'est désormais chose faite.
J'y ai consacré une [branche](https://git.framasoft.org/floreal/www.nimukaito.net/tree/tag_pages)
de développement. Et ai même demandé aux auteurs de nanoc[^5], s'ils voulaient bien m'aider à ajouter
cette même [fonctionalité](https://github.com/nanoc/nanoc/issues/803) dans leur logiciel afin que
d'autres puissent en profiter.

Voilà. Je vous laisse donc réagir

[^1]: Processus qui transforme un code source en fichier exécutable
      directement par le processeur d'un ordinateur.

[^2]: Choisir d'héberger un site ou d'autre service chez soi plutôt que sur
      une machine en datacenter

[^3]: J'ai eu d'ailleurs un cas de conscience lors de mon dernier article car
      j'y ai cité des marques.

[^4]: C'est de ces traces que se nourrissent les traqueurs de navigation, tout du moins
      celui que j'ai écrit pour mon ancien employeur.

[^5]: [Nanoc](http://nanoc.ws/) est le générateur de contenu statique qui me permet
      de maintenir ce site.
