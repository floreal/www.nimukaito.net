---
title: "Ma première mêlée"
author_name: floreal
created_at: 2016-08-31 20:03:51
tags:
  - Combat Médiéval
  - Histoire
kind: article
---

Après une \(\*hem\*\) longue période sans publication, voici un nouveau billet,
et ça ne parle pas d'informatique, cette fois-ci. J'ai un peu changé la
ligne éditoriale de mon blog d'ailleurs ! Bonne lecture !

Voilà un peu plus d'un an que je me suis mis au combat médiéval, que je
m'entraîne aussi assidûment que possible au maniement de 
l'[épée bâtarde](https://fr.wikipedia.org/wiki/%C3%89p%C3%A9e_b%C3%A2tarde),
c'est à dire l'épée « à une main et demi ».

![Épée Bâtarde](bastard-sword.jpg)

Le week-end dernier, j'ai pu participer au combat en mêlée, lors de la fête
médiévale de Miramas.
Voici donc mes impressions, de ma première expérience en armure.

## Le port de l'armure.

Malgré la chaleur de fin du mois d'août, sous 30Kg d'acier et de tissus,
il fait plutôt frais. En effet sous les pièces métalliques, le
[gambison](https://fr.wikipedia.org/wiki/Gambison) absorbe la sueur,
celle-ci s'évaporant permet de conserver une température qui m'a parue
légèrement inférieure à la température ambiante.

L'armure que j'ai portée était composée d'une Corasine (plastron constitué
de plusieurs plaques d'acier), de jambes sans grèves, de bras d'armes, et de
spalières et de gantelets tous faits de plaques d'acier entières, articulées
pour certaines, un coletin d'écailles ainsi qu'un bassinet en museau de chien.

![Armure (Crédit photo: Marty Mc Flaï)](armor.jpg)

Les mouvements sont dans une certaine mesure contrains sans être trop gênés. Le
poids des bras d'armes m'a donné l'impression d'avoir une épée qui ne pèse
rien en comparaison (Elle fait pourtant 1,6 Kg). Ce qui rendait le maniement
de mon arme difficile était au final le poids et l'épaisseur des bras d'arme.

Le déplacement en armure est, sans être difficile, lui aussi contraignant, et
j'ai du faire une longue pause pour reprendre mon souffle à l'entrée de la
lice, le chemin depuis le camp devient long quand on doit déplacer plus que
sa propre chair.

Enfin le casque que je portais me gênais moins que je ne l'aurais cru au
premier abord. Ce qui m'avait gêné avec les heaumes que j'avais précédemment
porté était davantage, le fait de moins bien entendre que de moins bien voir.

## Le combat

Les règles de cette mêlée sont simples:
- On a droit à être touchés 5 fois en suite on se met au sol (mort/neutralisé)
- Les estocs sont formellement interdits (réputés « dangereux[^1] »)
- Seul les coup fendant (du haut vers le bas) sont autorisés.

La mêlée se déroule en plusieurs manches. Un camp défend, l'autre attaque.
L'objectif des défenseurs est de protéger le seigneur; celui des attaquants
de l'atteindre.

Chaque manche se déroule en deux phases:
- Les archers défenseurs tirent deux volées de flèches, pendant que les attaquants
se protègent derrière leurs boucliers, les attaquants touchés sont neutralisés
- Les attaquants se lancent à l'assaut
- La manche se termine lorsque les attaquants ont atteint le seigneur ou que les
attaquants soient tous neutralisés.

### Première manche

Une fois que les archers se soient retirés, après leurs deux volées, étant peu
mobiles avec les armures que nous portions, mes deux comparses et moi (Dame Mélodie, et
Sieur Hilderic, tous deux suivant le même cursus que moi), nous sommes postés
en dernière ligne, en défense du seigneur. Les premières lignes des deux camps
s'affrontaient, puis, deux vikings qui sont parvenus à passer s'approchaient enfin
de moi. J'ai alors frappé, et touché le bouclier à ma droite. Tant pis, je
réarme, et frappe à la jambe[^2]. Je n'ai pu placer que ce coup là, avant de me
mettre au sol, ayant reçu 5 touches. Malgré tout nous avons remporté la première
manche.

### Seconde manche

Cette fois ci, je me suis retrouvé face à deux autres guerriers lourds (eux aussi issus de
mon école: Sieur Genséric et Dame Julie), j'ai eu plus de facilités à parer leurs coups et
j'ai pu, me semble-t'il leur en placer placer un ou deux, mais je me retrouvais alors moins
vif, et ma faible expérience du combat face à la leur, plus grande aura eu raison de moi, car
j'ai été touché cinq fois. J'étais alors épuisé et me relever seul m'était alors impossible.
J'étais à bout de souffle et j'ai du Ôter mon heaume. Je ne sais d'ailleurs pas qui a
remporté cette dernière manche.

### Fin des hostilités

Il n'y eut pas de troisième manche, et j'étais prêt à me retirer de la lice si elle avait du
avoir lieu car je n'étais plus en mesure de me battre. Je me souviens avoir entendu les
encouragements de mon professeur pendant la bataille.

## En conclusion

Avec le recul je me dis que j'aurais pu saisir, lors de la première manche, le bouclier du
Viking à droite pour le frapper au casque pendant que je gênais son camarade. Mais dans le feu de
l'action, je n'ai pas eu le temps d'y penser. J'en déduis que je manque d'entraînement face à des
« bouclier-épée-une-mains », car je n'ai pu me reposer sur des réflexes plus judicieux.

Je suis très content de mon premier vrai combat, puisque je ne pensais pas réussir à faire
une seule touche, gêné par le poids de l'armure. J'ai à présent une idée de mon niveau en
situation de combat, et je vais pouvoir axer mes entraînement futurs pour combler mes
faiblesses et renforcer mes maigres atouts.

### Remerciement

Je remercie donc l'ECM[^3] et ses membres, notre sergent pour
m'avoir permis de porter son armure, mes adversaires et coéquipiers lors de la mêlée, ainsi que
la ville de Miramas et les organisateurs de la fête médiévale, grâce à qui j'ai pu m'essayer
aux armes, en cette fin de mois d'août.

Crédit de la photo de l'armure: Marty Mc Flaï

[^1]: C'est dangereux en effet, quand on ne sait pas les donner ni les recevoir.

[^2]: Je ne suis souvenu après, coup que ce n'était pas autorisé. Du coup je ne sais pas
      si la touche était valable

[^3]: École de Combat Médiéval
