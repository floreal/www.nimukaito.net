---
title: "Réponse à Nostalgames"
author_name:
  - floreal
  - kivutar
created_at: 2016-10-15 18:16:39
tags:
  - Rétrogaming
  - Logiciel Libre
  - Droit de réponse
  - Éthique
kind: article
---

## Mise en contexte

Nostalgames, voulait commercialiser une console « Rétro-Gaming », ils avaient
lancé une campagne de financement participatif, récemment annulée par la
plate-forme qui l'hébergeait. En effet, suite à divers signalements de non
respect des licences des logiciels installés sur leur produit; les auteurs
des dits logiciels ayant choisi des licences prohibant leur usage à des fins
commerciales.

Étant donné que je contribue à [Lakka](http://www.lakka.tv) − une distribution
GNU/Linux entièrement libre et gratuite − qui s'appuie également sur ces
logiciels (En réalité Lakka est même la distribution officielle de
[libretro](http://www.libretro.com), à l'origine de ces logiciels), j'ai voulu
répondre au [communiqué](http://www.twitlonger.com/show/n_1sp6mg3) qu'ils ont
récemment publié. Pour cela j'ai demandé à Kivutar, le fondateur et développeur
principal de Lakka, de se joindre à moi pour écrire ce billet. Nous allons
exposer notre point de vue sur Nostalgames, leur projet de console
« RetroPac ».

## Notre réponse

> RetroPac, une console de jeux clé en main qui met l’intelligence collective
> à la portée de tous.
>
> Tous les gamers ne sont pas des informaticiens et développeurs chevronnés…
> Dans les années 1990 c’était même un des principaux points de différenciation
> entre les adeptes de la console et du PC ! C’est justement à l’usage des
> premiers que le RetroPac souhaite s’adresser.

Lakka aussi. Par contre ça demande quelques efforts triviaux au lieu de quelques
deniers supplémentaire pour financer l'assemblage et justifier la marge facile
que se serait fait Nostalgames. Le truc c'est que la grosse partie du travail
se situe du coté des logiciel. Retroarch et libretro se sont construits sur
plusieurs années, avec de nombreux efforts, le tout sans être rémunérés parce
qu'il y avait une volonté de partage et d'entraide, de bienveillance entre les
contributeurs/utilisateurs.

> Émulateurs et jeux rétros
>
> Les fans de jeux vidéo anciens, privés de leurs consoles obsolètes, ont
> développé des « émulateurs » permettant de transcrire d’anciens jeux sur PC.
> La  plupart de ces émulateurs sont aujourd’hui disponibles en ligne,
> gratuitement, et régis par le code des logiciels libres (GPL v3).

On ne parle pas de code des logiciels libre à moins qu'il s'agisse du code
source, mais plutôt d'éthique. Sous l'ange légal on parle de Licence. GPL v3
en est une de ces licences mais il en existe une pléthore. Certains plus
permissives que d'autres, et d'autres plus restrictive, notamment en ce qui
concerne la commercialisation. Nous verrons plus loin que c'est là que le bas
blesse.

> Ainsi, aujourd’hui certains gamers sont capables d’assembler eux-mêmes, chez
> eux, les composants d’une « console » permettant de rejouer aux jeux vidéo
> des années 1970 à 2000. Mais cette pratique est réservée aux initiés…

Assembler vous même une console de jeu est bien plus facile qu'il n'y parait,
vous seriez surpris ! Toutefois, il est vrai que ça reste trop compliqué pour
les gens qui n'ont pas d'ordinateur, ou pour les enfants non accompagnés. Donc
l'idée d'avoir un produit bien finalisé sur le marché est bonne. Ce qui pose
souci, c'est la loi. Premièrement, les émulateurs permettant de jouer à la
SuperNES et la Megadrive sont sous licence « Non Commercial ». Ça veut dire que
les auteurs refusent que leur code soit utilisé dans un produit vendu. Ils font
ça pour se protéger des boites comme Nintendo et SEGA qui pourraient les
attaquer pour le manque à gagner. Toutefois, si tous les auteurs tombent
d'accord, ils peuvent octroyer une autorisation spéciale pour la vente. Mais il
est quasiment impossible d'atteindre un tel accord au vu du nombre d'auteurs,
qui dépasse les 20. Certains sont totalement opposés au commerce et c'est leur
droit le plus naturel.

> C’est pour répondre à ce besoin que Nostalgames s’est lancé dans la
> production de le RetroPac, une console clé en main, composée d’une carte mère
> Raspberry Pi3, hardware largement distribué sur laquelle a été installé un
> système d’exploitation spécifiquement créé : NostalOS. Ce système
> d’exploitation est une distribution propre de Linux, complétée avec d’autres
> logiciels open-source, selon leurs licences et les accords conférés lorsque
> nécessaire.

Floréal les avait contactés à ce sujet, pourquoi faire leur propre OS alors que
Lakka existe et fonctionne très bien? Pourquoi ne l'ont-ils pas choisi? Il eût
été plus judicieux d'inclure cette dernière, et de contribuer à son
développement, par exemple en corrigeant des bugs, s'ils n'ont pas le bagage
technique pour, ouvrir des tickets pour les signaler, etc.

Lakka est évoqué mais ça aurait pu être des tas d'autres projets similaires:
RetroPie, RecallBox, LibreElec + Retroarch AddOn, etc.

Donc en gros, ces logiciels existent déjà, et au lieu d'y contribuer, Nostalgames
préfère réinventer la roue. Pourquoi ? Quand on lit leurs premières
interventions sur les réseaux sociaux, on peut voir qu'ils comptaient fermer
le code de leur produit. C'est une stratégie pour se protéger, pour que
personne ne puisse copier leur projet. En gros, ils sont d'accord pour utiliser
le travail des autres, par contre, pas d'accord pour que les autres utilisent le
leur !

> A noter, il n’y a aucune Tivoisation sur le RetroPac ; l’utilisateur est
> libre d’utiliser sa console de la manière dont il le souhaite car l’accès au
> Shell n’est pas verrouillé.

La tivoisation est la création d'un système qui inclut des logiciels libres,
mais utilise le matériel électronique pour interdire aux utilisateurs d'y
exécuter des versions modifiées. Merci
[Wikipedia](https://fr.wikipedia.org/wiki/Tivoisation). C'est une bonne chose
qu'ils ne s'y soient pas essayés. Ça aurait violé la majorité des licenes.

> Droit d’utilisations des logiciels utilisés
> Pour les émulateurs sous licence GPLv3, l’utilisation commerciale est
> permise. C’est notamment le cas de Libretro/RetroArch, le plus connu d’entre
> eux.
>
> Au delà du respect de la propriété intellectuelle, Nostalgames comprend que
> l’écosystème des logiciels libres a besoin de ressources pour se développer,
> et réitère son engagement à libérer des fonds pour les aider.

Pour l'instant il n'y a pas eu d'acte. Comme dit plus haut, Nostalgames
n'a montré aucun signe de collaboration avec les équipes qui ont développé la
base de leur produit. Seulement de belles paroles. Et puis, ce n'est pas des
fonds dont nous avons besoin mais de contribution, de
[don de matériel](http://www.lakka.tv/doc/Wishlist/), et les dons financiers
sont exclusivement affectés à ces derniers[^1]. En ce qui nous concerne,
nous faisons du logiciel libre non rémunéré dans une optique qualitative. Nous
ne voulons pas avoir à faire une impasse sur une fonctionnalité utile ou juste
sympa ou sur la qualité intrinsèque de ces projets, ce qui est incompatible
avec la logique financière de retour sur investissement; et avoir à rendre des
comptes à des gens qui ont des attentes légitimées par des contributions
pécuniaires. En d'autre termes nous souhaitons vivre pour faire du Logiciel
libre et pas faire du logiciel libre pour en vivre.

Et surtout, la communauté n'a pas besoin de RetroPac pour se développer. Un
projet commercial qui ne respecte pas les licences et en fait la dernière chose
dont nous aurions besoin.

> Concernant le SNES9x 2002 (logiciel à utilisation non-commerciale), également
> utilisé dans la console, Nostalgames a contacté les personnes citées dans le
> fichier de licence et a obtenu l'accord explicite des ayants droits sur cette
> version. Les autres personnes listées dans le fichier de licence sont citées
> pour leur participation au code (qui est disponible par ailleurs sous GPLv2,
> réadapté par les deux créateurs).

C'est faux. Il en va de même avec les auteurs de 
[GensPlusGX](https://github.com/ekeeke/Genesis-Plus-GX/issues/109). Tous les
auteurs doivent être contactés, et cela relève de l'impossible, car tous les
auteurs ne sont pas forcément d'accord.

> Commercialisation et intelligence collective
>
> Le RetroPac met à disposition de tous des technologies jusque-là réservées
> à une population d’initiés afin de démocratiser la pratique du retrogaming.

On trouve des cartes-PC pas chères chez des enseignes grand public. De plus, si
le retrogaming à base d'émulation est réservé à une population d'initié, c'est
pour une raison : la loi est peu claire à ce sujet. L'émulation n'est pas
forcément illégale en soit, mais c'est limite, et une jurisprudence défavorable
à l'émulation serait fort dommageable et criminaliserait de fait les auteurs et
les utilisateurs d'émulateurs.

> Le coût de la console prend ainsi en charge l’achat des composants (tous 
> disponibles en vente libre), leur assemblage, l’intégration des logiciels
> nécessaires à l’exploitation des jeux, ainsi que tous les coûts inhérents au
> bon fonctionnement d’une entreprise. La plus-value de la console réside dans
> la création d’un système d’exploitation propre, NostalOS, et de la facilité
> de prise en main d’une console certifiée CE et garantie 2 ans.

Oui mais comme dit plus tôt, il y a tout le travail fourni par la communauté
rassemblée autour de Retroarch et libretro. Nous considérons davantage que la
plus-value se situe là plutôt que dans l'achat, l'assemblage - à la porté de
n'importe qui - et la revente de matériel. Si Retroarch et libretro ou tout
autre projet similaire n'existait pas, le RetroPac n'aurait jamais vu le jour,
puisque comme Nostalgames le dit, NostalOS est composé de ces logiciels.

> Le RetroPac est ainsi destinée à la communauté des gamers « non geeks » qui
> ne sont pas en mesure d’assembler eux-mêmes leur console et favorise ainsi
> la diffusion des logiciels libres hors de la communauté des développeurs.

Encore une fois, ce n'est pas du tout compliqué, et il nous parait très
condescendant de considérer une élite « Geek » face à une plèbe « non geek ». 
Vouloir créer un tel clivage n'a pas de sens. Il faut arrêter d'effrayer les
gens avec la technologie « qui est trop compliquée » et de les rabaisser au
rang de simples consommateurs ignares. C'est très irrespectueux. L'entraide
est une meilleure solution que le « tout fait, tout cuit ». C'est la vision
qui nous a poussé à faire de Lakka ce qu'elle est devenue. On voulait quelque
chose de facile à utiliser, on l'a fait, et maintenant on le partage.
Si vous n'êtes pas en mesure de monter vous même votre console de jeu, vous
pouvez demander à un ami ou un cousin calé en informatique de vous montrer
comment faire. Une fois qu'on connaît Lakka ou Recalbox, ça prend moins de 20
minutes pour avoir le même résultat. Sauf que cette fois, c'est respectueux
du point de vue des licences.

> Nostalgames a travaillé avec ses conseillers juridiques et s’engage à prendre
> en considération tous les questionnements de propriété intellectuelle mais
> également de qualité du RetroPac, dans le désir de faire progresser le projet
> et d’avancer dans un esprit collaboratif.

Nostalgames aurait mieux fait de travailler avec les contributeurs plutôt qu'avec
des organismes légaux, les choses se seraient mieux passé, comme pour la
[gamegirl](https://hackaday.io/project/10207-gamegirl-the-retro-console-done-right),
par exemple, qui nourrit des ambitions similaires, mais qui est plus proche de la
communauté, et dont l'auteur a pas mal contribué à libretro et Lakka.

Mais revenons à Nostalgames. On voit plutôt ici la mise en place d'une protection
juridique, qu'une volonté réelle de faire du collaboratif. De plus, leurs
juristes ne devaient probablement pas avoir tous les éléments à leur disposition
pour répondre convenablement à leurs demandes. En fait, nous comprenons mal
comment on a pu conseiller à ces étudiants de commercialiser un produit basé sur
l'émulation.

Au final C'est un énorme gâchis en terme de temps et d'énergie pour Nostalgames
ainsi que pour l'équipe de Retroarch, qui aurait pu être employé à travailler
sur leurs projets, tout ça parce que de jeunes commerciaux ont été mal conseillés,
et ne se sont pas rapprochés des bonnes personnes. Est-ce là un problème
culturel ?

## En conclusion

De nombreux projets semblables a « RétroPack » ont été tentés. La plupart sont
morts dans l'œuf comme ce dernier. Certains ont marché, et arrivent à vivoter.

Malgré les reproches que nous avons fait à Nostalgames au court de ce billet,
nous reconnaissons tout de même qu'ils ont, au fil de leur projet, tenté de
corriger leurs erreurs. Ils ont été ambitieux, mais ont choisi une voie qui,
dans ce contexte, ne pouvait qu'échouer.

Leur première grosse faute c'est d'avoir prétendu révolutionner le monde du
retrogaming, en ventant leur produit a eux, en oubliant que tout le travail
qu'ils n'ont pas fait, c'est une communauté de passionnés qui l'a fait pour eux.
Ce manque de reconnaissance leur a valu une grosse levée de bouclier de
l'équipe Retroarch.

La seconde, c'est d'avoir voulu être procéduriers, et se blinder juridiquement.
Forcément des choses leur ont échappé et l'équipe Retroarch, déjà hostile à se
projet s'est engouffré pans ces failles.

C'est juste dommage, s'ils avaient mieux conseillés, RetroPack aurait
certainement marché. seulement ils n'ont pas fait le plus important: Contribuer
et S'intégrer à une communauté. Si seulement ils avaient eu de meilleurs
conseils de leurs professeurs.

Enfin, nous souhaitons adresser de sincère remerciements à l'équipe de
[KissKissBankBank](https://www.kisskissbankbank.com/) d'avoir réagi avec
justesse aux signalements qui leur ont été soumis, et pour leur bienveillance
vis-à-vis des communautés rassemblées autour du Logiciel Libre.

[^1]: Nous avons finalement accepté les dons financiers car des utilisateurs
      nous l'ont demandés car ils n'avaient pas d'autres moyens de contribuer
      au projet, alors que nous y étions opposés.
