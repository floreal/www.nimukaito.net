# frozen_string_literal: true

def authors(item)
  [item[:author_name]].flatten
end
