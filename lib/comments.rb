# frozen_string_literal: true

# Will insert external comment app "isso"
module Comments
  ISSO_URL = 'isso.url'

  def comments_base_url
    @comments_base_url = File.read(ISSO_URL).rstrip if @comments_base_url.nil?
    @comments_base_url
  end

  def comments_section
    '<section id="isso-thread"></section>'
  end

  def comments_script
    <<-HTML
          <script data-isso="#{comments_base_url}/"
            data-isso-lang="fr"
            src="#{comments_base_url}/js/embed.min.js"></script>
    HTML
  end
end

include Comments
