# frozen_string_literal: true

require 'i18n'

# Helper allowing to localize date and time
module Localization
  # Localises a given date
  def localize_date(date)
    setup_locales unless locales_loaded?
    I18n.l date, format: :long
  end

  # Checks wheter locale is loaded or not
  def locales_loaded?
    !I18n.load_path.empty?
  end

  # Configures locale
  def setup_locales
    I18n.load_path = Dir.glob(locale_config[:pattern])
    I18n.default_locale = locale_config[:default].to_sym
  end

  # Returns locale section configuration
  def locale_config
    @config[:locale]
  end
end

include Localization
