# frozen_string_literal: true

require 'erb'
require 'to_slug'

# Provides feature related to page tagging
#
module TagPages
  include ERB::Util

  # finds out every item containing tags
  # @return Array an array of Nanoc::Item
  def tagged_items
    @items.select do |item|
      item[:tags].is_a? Array
    end
  end

  # finds each tags from every page. Each tag appears once in the result
  # which is sorted
  # @return Array an array of String (tag)
  def all_tags
    # keeps from rebuilding tag list each time this method is called
    return @all_tags unless @all_tags.nil?

    tags = []
    tagged_items.each do |item|
      tags += item[:tags]
    end
    @all_tags = tags.uniq.sort
  end

  # finds all tags for every pages and create a link for each
  #
  # @return String
  def all_tag_links(attributes = {})
    all_tags.map { |tag| tag_link tag, attributes }
  end

  # finds out what should tag pages base path should be
  # will read configuration in nanorc.yml file or provide
  # a default path (/tags)
  # @example Configuring tag prefix
  #
  #   tag_pages:
  #     prefix: /keywords
  #
  # @return String the tag page prefix
  def tag_pages_prefix
    default = '/tags'
    tag_pages_conf = @config[:tag_pages]
    return @config[:tag_pages][:prefix] || default unless tag_pages_conf.nil?
    default
  end

  # provide a link to a tag page. Uses #tag_page_pefix to build the tag page
  # path
  #
  # @see #tag_url
  def tag_link(tag, attributes = {})
    link_to tag, @items[tag_url(tag)], attributes
  end

  # Same as Nanoc::Helpers::Tagging#tags_for, but provides a list of link
  # to the tag pages
  #
  # @return String
  def tag_links_for(item, separator = ' ', links_attributes = {})
    item[:tags].map { |tag| tag_link tag, links_attributes }&.join(separator)
  end

  # Creates one Nanoc::Item for each tag
  #
  # @see #tag_url
  # @see #all_tags
  def create_tags_pages
    all_tags.map do |tag|
      @items.create(
        "<% items_with_tag(\"#{tag}\").each do |post| %>
             <%= render '/partials/article-card.html', post: post %>
         <% end %>",
        { title: "Tag: #{tag}" },
        tag_url(tag)
      )
    end
  end

  # Provides an url for a given tag
  #
  # @see #tag_pages_prefix
  # @return String
  def tag_url(tag)
    "#{tag_pages_prefix}/#{tag.to_slug}.html"
  end
end

include TagPages
